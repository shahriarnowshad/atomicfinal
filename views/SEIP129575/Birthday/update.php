<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP129575\Birthday\Birthday;
use App\Bitm\SEIP129575\Utility\Utility;

$birthday= new Birthday();
$birthday->prepare($_POST)->update();