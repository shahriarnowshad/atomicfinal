<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP129575\Birthday\Birthday;
use App\Bitm\SEIP129575\Utility\Utility;

$birthday=new Birthday();
$singleBday= $birthday->prepare($_GET)->view();
$newDate = date("d-m-Y", strtotime($singleBday->birthday));

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css"/>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?php echo $singleBday->name ?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleBday->id?></li>
        <li class="list-group-item">Name: <?php echo $singleBday->name ?></li>
        <li class="list-group-item">Date: <?php echo $newDate ?></li>

    </ul>
</div>

</body>
</html>


