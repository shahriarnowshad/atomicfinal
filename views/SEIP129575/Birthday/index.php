
<?php
session_start();
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP129575\Birthday\Birthday;
use App\Bitm\SEIP129575\Message\Message;
$birthday = new Birthday();
//$bdData = $birthday->index();


if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$birthday->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$bdData=$birthday->paginator($pageStartFrom,$itemPerPage);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Birth Day</title>
    <link href="../../../Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

</head>
<body>
<div class="container">
    <h2>Birthday List</h2>
    <a class="btn btn-info" href="create.php">Add Birth Date</a>
    <a class="btn btn-info" href="trashed.php">Trash List</a>
    <div id="message">
        <?php
        if (isset($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>

    <form role="form">
        <div class="form-group">
            <label for="sel1">Select homw many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option>5</option>
                <option>10</option>
                <option selected>15</option>
                <option>20</option>
                <option>25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>


  <table class="table">
                    <thead>
                    <tr>
                        <th>#SL</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Birth Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sl = 0;
                    foreach ($bdData as $bd){
                        $sl++;
                        $newDate = date("d-m-Y", strtotime($bd->birthday));
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $bd->id; ?></td>
                            <td><?php echo $bd->name; ?></td>
                            <td><?php echo $newDate; ?></td>
                            <td>
                                <a href="view.php?id=<?php echo $bd->id;  ?>" class="btn btn-info">View</a>
                                <a href="edit.php?id=<?php echo $bd->id; ?>" class="btn btn-primary">Edit</a>
                                <a href="delete.php?id=<?php echo $bd->id; ?>" class="btn btn-danger" id="delete">Delete</a>
                                <a href="trash.php?id=<?php echo $bd->id; ?>" class="btn btn-info">Trash</a>
                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
    <ul class="pagination">
        <li><a href="#">Prev</a></li>
        <?php echo $pagination?>
        <li><a href="#">Next</a></li>
    </ul>

</div>


<script type="text/javascript">
    $('#message').show().delay(2000).fadeOut();


    $(document).ready(function(){
        $("#delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
    });
</script>
</body>
</html>