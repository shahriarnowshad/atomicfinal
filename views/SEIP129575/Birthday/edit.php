<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP129575\Birthday\Birthday;
use App\Bitm\SEIP129575\Message\Message;
$birthday = new Birthday();
$singleBday= $birthday->prepare($_GET)->view();
$newDate = date("d-m-Y", strtotime($singleBday->birthday));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../Resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../Resource/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

</head>
<body>

<div class="container">
    <h2>Birthday</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>ID</label>
            <input type="text" name="id" class="form-control" value="<?php echo $singleBday->id ?>">
            <label>Name</label>
            <input type="text" name="name" class="form-control"  value="<?php echo $singleBday->name ?>">
            <label>Date</label>
            <input type="date" name="date" class="form-control" value="<?php echo $singleBday->birthday ?>" >
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</div>

</body>
</html>