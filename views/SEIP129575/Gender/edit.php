<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP129575\Gender\Gender;

$gender = new Gender();
$single=$gender->prepare($_GET)->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css"/>
    <scritp src="../../../Resource/bootstrap/js/bootstrap.min.js"></scritp>
</head>
<body>

<div class="container">
    <h2>Gender</h2>
    <form role="form" action="update.php" method="post">
        <label>ID</label>
        <input type="text" class="form-control" name="id"  value="<?php echo $single->id ?>"/>
        <label>Name</label>
        <input type="text" class="form-control" name="name" " value="<?php echo $single->name ?>"/>
        <div class="radio">
            <label class="radio-inline"><input type="radio" value="Male" name="gender" <?php if($single->gender==="Male")echo "checked";  ?> >Male</label>
        </div>
        <div class="radio">
            <label class="radio-inline"><input type="radio" value="Female" name="gender"<?php if($single->gender==="Female")echo "checked";  ?>  >Female</label>
        </div>
        <div class="radio">
            <label class="radio-inline"><input type="radio" value="Other" name="gender" <?php if($single->gender==="Other")echo "checked";  ?>>Other</label>
        </div>
        <input type="submit" class="btn btn-info" value="Submit"/>
    </form>
</div>

</body>
</html>

