<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP129575\Gender\Gender;

$gender = new Gender();
$gender->prepare($_POST)->store();