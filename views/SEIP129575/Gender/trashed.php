<?php
session_start();
include_once ('../../../vendor/autoload.php');
//var_dump($_POST);


use App\Bitm\SEIP129575\Gender\Gender;
use App\Bitm\SEIP129575\Utility\Utility;
use App\Bitm\SEIP129575\Message\Message;
$gender = new Gender();
$all=$gender->trashed();



?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css"/>
    <scritp src="../../../Resource/bootstrap/js/bootstrap.min.js"></scritp>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>


</head>
<body>

<div class="container">
    <h2>All Person</h2>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <form action="recovermultiple.php" method="post" id="multiple">
        <a href="index.php" class="btn btn-primary" role="button">See All List</a>
        <button type="submit"  class="btn btn-primary">Recover Selected</button>
        <button type="button"  class="btn btn-primary" id="multiple_delete">Delete Selected</button>


        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Check Item</th>
                    <th>SL#</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sl=0;
                foreach ($all as $g){
                    $sl++?>
                    <tr>
                        <td><input type="checkbox" name="mark[]" value="<?php echo $g->id?>"></td>
                        <td><?php echo $sl?></td>
                        <td><?php echo $g->id ?></td>
                        <td><?php echo $g->name ?></td>
                        <td><?php echo $g->gender ?></td>
                        <td><a href="view.php?id=<?php echo $g->id ?>" class="btn btn-info" role="button">View</a>
                            <a href="edit.php?id=<?php echo $g->id ?>" class="btn btn-primary" role="button">Edit</a>
                            <a href="delete.php?id=<?php echo $g->id ?>" class="btn btn-danger delete" role="button"  >Delete</a>
                            <a href="recover.php?id=<?php echo $g->id ?>" class="btn btn-info" role="button">Recover</a>
                        </td>
                    </tr>
                <?php }?>

                </tbody>
            </table>
        </div>
    </form>
</div>
<script type="text/javascript">
    $('#message').show().delay(2000).fadeOut();
    $(document).ready(function(){
        $(".delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
        $('#multiple_delete').on('click',function(){
            document.forms[0].action="deletemultiple.php";
            $('#multiple').submit();
        });
    });
</script>

</body>
</html>

