
<?php
include_once "../../../vendor/autoload.php";
use App\Bitm\SEIP129575\Summary\Summary;
use App\Bitm\SEIP129575\Message\Message;
$summary = new Summary();
$all = $summary->trashed();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Company Summery</title>
    <link href="../../../Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

</head>
<body>
<div class="container">
    <h2>Summary List</h2>

    <form action="recovermultiple.php" method="post" id="multiple">
        <a href="index.php" class="btn btn-primary" role="button">See All List</a>
        <button type="submit"  class="btn btn-primary">Recover Selected</button>
        <button type="button"  class="btn btn-primary" id="multiple_delete">Delete Selected</button>
    <div id="message">
        <?php
        if (isset($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>


    <table class="table">
        <thead>
        <tr>
            <th>Check</th>
            <th>#SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>Summary</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl = 0;
        foreach ($all as $smry){
            $sl++;

            ?>
            <tr>
                <td><input type="checkbox" name="mark[]" value="<?php echo $smry->id?>"></td>
                <td><?php echo $sl; ?></td>
                <td><?php echo $smry->id; ?></td>
                <td><?php echo $smry->name; ?></td>
                <td><?php echo $smry->summary ?></td>
                <td>
                    <a href="view.php?id=<?php echo $smry->id;  ?>" class="btn btn-info">View</a>
                    <a href="edit.php?id=<?php echo $smry->id; ?>" class="btn btn-info">Update</a>
                    <a href="delete.php?id=<?php echo $smry->id; ?>" class="btn btn-danger" id="delete">Delete</a>
                    <a href="recover.php?id=<?php echo $smry->id; ?>" class="btn btn-info">Recover</a>
                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>

</div>


<script type="text/javascript">
    $('#message').show().delay(2000).fadeOut();


    $(document).ready(function(){
        $("#delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
        $('#multiple_delete').on('click',function(){
            document.forms[0].action="deletemultiple.php";
            $('#multiple').submit();
        });
    });

</script>
</body>
</html>