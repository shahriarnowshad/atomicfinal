<?php
include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP129575\Summary\Summary;

$summary=new Summary();
$single= $summary->prepare($_GET)->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../Resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../Resource/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

</head>
<body>

<div class="container">
    <h2>Company Summary</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>ID</label>
            <input type="text" name="id" class="form-control" value="<?php echo $single->id ?>">
            <label>Name</label>
            <input type="text" name="name" class="form-control"  value="<?php echo $single->name ?>">
            <label>Company Summary</label>
            <textarea  name="summary" class="form-control"  ><?php echo $single->summary ?></textarea>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</div>

</body>
</html>
