<?php
include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP129575\Summary\Summary;

$summary=new Summary();
$single= $summary->prepare($_GET)->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css"/>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?php echo $single->name ?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $single->id?></li>
        <li class="list-group-item">Name: <?php echo $single->name ?></li>
        <li class="list-group-item">Summery : <?php echo $single->summary ?></li>

    </ul>
</div>

</body>
</html>
