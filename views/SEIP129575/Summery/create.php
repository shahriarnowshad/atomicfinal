<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../../Resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../Resource/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

</head>
<body>

<div class="container">
    <h2>Atomoic Project- Email</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label>Company Name</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Company Name">
            <label>Company Summary</label>
            <textarea name="summary" class="form-control" id="summary" placeholder="Company Summery"></textarea>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>