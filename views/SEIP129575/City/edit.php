<?php
include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP129575\City\City;

$city= new City();
$single=$city->prepare($_GET)->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css"/>
    <scritp src="../../../Resource/bootstrap/js/bootstrap.min.js"></scritp>
</head>
<body>

<div class="container">
    <h2>User City</h2>

    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>ID</label>
            <input type="text" class="form-control" name="id" value="<?php echo $single->id ?>"/>
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="<?php echo $single->name ?>"/>
            <label>City</label>
            <select name="city" class="form-control" id="city">
<!--                <option style="display: none" selected disabled value="--><?php //$single->city ?><!--">--><?php //echo $single->city ?><!--</option>-->
                <option value="Chittagong" <?php if("Chittagong"===$single->city)echo "selected"; ?>>Chittagong</option>
                <option value="Dhaka" <?php if("Dhaka"===$single->city)echo "selected";?> >Dhaka</option>
                <option value="Comilla" <?php if("Comilla"===$single->city)echo "selected";?>>Comilla</option>
                <option value="Rajshahi" <?php if("RajShahi"===$single->city)echo "selected";?>>Rajshahi</option>
            </select>
            <input type="submit" class="btn btn-primary" value="Submit" />

        </div>
    </form>
</div>
</body>
</html>

