<?php
session_start();
include_once ('../../../vendor/autoload.php');
//var_dump($_POST);


use App\Bitm\SEIP129575\ProfilePicture\ImageUploader;
use App\Bitm\SEIP129575\Utility\Utility;
use App\Bitm\SEIP129575\Message\Message;
$profilePicture = new ImageUploader();

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$profilePicture->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$allInfo=$profilePicture->paginator($pageStartFrom,$itemPerPage);
//$allInfo=$profilePicture ->index();




?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All User Info</h2>

    <a href="create.php" class="btn btn-info" role="button">Add User Info</a> <a href="trashed.php" class="btn btn-primary" role="button">View Trashed Item</a><br><br>

    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select homw many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option>5</option>
                <option>10</option>
                <option selected>15</option>
                <option>20</option>
                <option>25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Profile Picture</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach ($allInfo as $info){
                $sl++?>
                <tr>
                    <td><?php echo $sl?></td>
                    <td><?php echo $info->id ?></td>
                    <td><?php echo $info->name ?></td>
                    <td><img src="../../../Resource/Images/<?php echo $info->images?>" alt="image" height="100px" width="100px"></td>
                    <td><a href="view.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">View</a>
                        <a href="edit.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $info->id ?>" class="btn btn-danger" role="button">Delete</a>
                        <a href="trash.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">Trash</a>
                    </td>
                </tr>
            <?php }?>

            </tbody>
        </table>
    </div>
    <ul class="pagination">
        <li><a href="#">Prev</a></li>
        <?php echo $pagination?>
        <li><a href="#">Next</a></li>
    </ul>
</div>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>

