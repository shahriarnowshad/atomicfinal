<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP129575\ProfilePicture\ImageUploader;

$profilePicture= new ImageUploader();
$profilePicture->prepare($_GET)->recover();