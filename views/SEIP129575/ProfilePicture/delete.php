<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP129575\ProfilePicture\ImageUploader;

$profilePicture= new ImageUploader();
$singleInfo= $profilePicture->prepare($_GET)->view();
unlink($_SERVER['DOCUMENT_ROOT'].'/AtomicProjectB20/Resource/Images/'.$singleInfo->images);
$profilePicture->prepare($_GET)->delete();