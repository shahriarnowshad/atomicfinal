<?php
session_start();
include_once ('vendor/autoload.php');
//var_dump($_POST);
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="Resource/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="Resource/style.css"/>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body class="design">
<div class="container">
    <div>
        <h1 class="heading">Atomic Projects Home</h1>
    </div>
    <div class="jumbotron">
        <h3>Project Navigation Links:</h3>
        <div class="row">
            <div class="col-lg-12">
                <a href="views/SEIP129575/Book/" class="btn btn-info">Book Title</a>
                <a href="views/SEIP129575/Birthday/" class="btn btn-warning">Birthday</a>
                <a href="views/SEIP129575/Gender/" class="btn btn-success">Gender</a>
                <a href="views/SEIP129575/City/" class="btn btn-danger">City</a>
                <a href="views/SEIP129575/Email/" class="btn btn-warning">Email</a>
                <a href="views/SEIP129575/Hobby/" class="btn btn-info">Hobby</a>
                <a href="views/SEIP129575/ProfilePicture/" class="btn btn-success">Profile Picture</a>
                <a href="views/SEIP129575/Summery/" class="btn btn-danger">Org Summary</a>
            </div>
        </div>
    </div>
    <div class="jumbotron">
        <p>Name: Shahriar Mohammad Nowshad<br />
            ID: SEIP129575<br />
            Web Development PHP Batch-20</p>
    </div>
</div>


</body>
</html>
