<?php
namespace App\Bitm\SEIP129575\Gender;
use App\Bitm\SEIP129575\Message\Message;
use App\Bitm\SEIP129575\Utility\Utility;

class Gender{
    public $id="";
    public $name="";
    public $gender="";
    public $conn="";

    public function prepare($data="")
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("gender", $data)) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        //Utility::dd( $this);
        return $this;

    }
    public function __construct(){
        $this->conn= mysqli_connect("localhost","root","","atomicprojectb20") or die("Database connection establish failed");
    }
    public function store(){
        $query="INSERT INTO `atomicprojectb20`.`gender` (`name`, `gender`) VALUES ('".$this->name."','".$this->gender."')";
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');

        }

    }

    public  function index(){
        $_allgender=array();
        $query= "SELECT * FROM `gender` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allgender[]=$row;
        }
        return $_allgender;
    }

    public function view(){
        $query="SELECT * FROM `gender` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }

    public function update(){
        $query="UPDATE `atomicprojectb20`.`gender` SET `name`='".$this->name."', `gender` = '".$this->gender."' WHERE `gender`.`id` ='".$this->id."'";
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been updated successfully.
</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="Delete from `atomicprojectb20`.`gender` where `gender`.`id`='".$this->id."'";
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted.
</div>");
            Utility::redirect('index.php');

        }
    }

    public function trash()
    {
        $this->deleted_at = time();

        $query = "UPDATE `atomicprojectb20`.`gender` SET `deleted_at` =".$this->deleted_at." WHERE `gender`.`id` = ".$this->id;
        echo $query;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
              <strong>Deleted!</strong> Data has been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
              <strong>Deleted!</strong> Data has not been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allBook = array();
        $query = "SELECT * FROM `gender` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;


    }

    public function recover()
    {

        $query = "UPDATE `atomicprojectb20`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` = " . $this->id;
        echo $query;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb20`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` IN(" . $ids . ")";

            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb20`.`gender` WHERE `gender`.`id` IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb20`.`gender` ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `gender` LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }

}