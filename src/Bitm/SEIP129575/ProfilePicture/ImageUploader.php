<?php
namespace App\Bitm\SEIP129575\ProfilePicture;
use App\Bitm\SEIP129575\Message\Message;
use App\Bitm\SEIP129575\Utility\Utility;


class ImageUploader{
    public $id="";
    public $name="";
    public $image_name="";
    public $conn;


    public function __construct(){
        $this->conn= mysqli_connect("localhost","root","","atomicprojectb20") or die("Database connection establish failed");
    }

    public function prepare($data=Array())
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("image", $data)) {
            $this->image_name = $data['image'];
            echo $this->image_name;
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }


    public function store(){
        $query="INSERT INTO `atomicprojectb20`.`profilepic` (`name`, `images`) VALUES ('{$this->name}', '{$this->image_name}')";
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect('index.php');
        }
        else {
            Message::message("Data has not been stored successfully");
            Utility::redirect('index.php');
        }
    }

    public  function index(){
        $_allInfo=array();
        $query= "SELECT * FROM `profilepic` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allInfo[]=$row;
        }
        return $_allInfo;
    }

    public function view(){
        $query="SELECT * FROM `profilepic` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }

    public function update(){
        if(!empty($this->image_name)) {
            $query = "UPDATE `atomicprojectb20`.`profilepic` SET `name` = '{$this->name}', `images` = '{$this->image_name}' WHERE `profilepic`.`id` =" . $this->id;
        }else{
            $query = "UPDATE `atomicprojectb20`.`profilepic` SET `name` = '{$this->name}' WHERE `profilepic`.`id` =" . $this->id;
        }

        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect('index.php');
        }
        else {
            Message::message("Data has not been updated successfully");
            Utility::redirect('index.php');
        }
    }

    public function delete(){
        $query= "DELETE FROM `atomicprojectb20`.`profilepic` WHERE `profilepic`.`id` = ".$this->id;

        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect('index.php');
        }
        else {
            Message::message("Data has not been deleted successfully");
            Utility::redirect('index.php');
        }

    }
    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb20`.`profilepic` SET `deleted_at` =" . $this->deleted_at . " WHERE `profilepic`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been trashed successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allprofilepic = array();
        $query = "SELECT * FROM `profilepic` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allprofilepic[] = $row;
        }

        return $_allprofilepic;


    }

    public function recover()
    {

        $query = "UPDATE `atomicprojectb20`.`profilepic` SET `deleted_at` = NULL WHERE `profilepic`.`id` = " . $this->id;
        echo $query;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb20`.`profilepic` SET `deleted_at` = NULL WHERE `profilepic`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-warning\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb20`.`profilepic` WHERE `profilepic`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb20`.`profilepic` ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `profilepic` LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }








}
